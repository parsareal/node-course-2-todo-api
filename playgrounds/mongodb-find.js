// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

// var obj = new ObjectID();
// console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err){
    // return console.log('Unable to connect to mongodb server ...');
    return console.log(err);
  }

  console.log('Connect to Mongodb server ...');

  // db.collection('Todos').find().toArray().then((res) => {
  //   console.log('Todos');
  //   console.log(JSON.stringify(res, undefined, 2));
  // }, (err) => {
  //   console.log('Unable to fetch todos', err);
  // });

  // db.collection('Todos').find().count().then((count) => {
  //   console.log(`Todos count: ${count}`);
  // }, (err) => {
  //   console.log('Unable to fetch todos', err);
  // });

  db.collection('Users').find({name: 'mohammad'}).toArray().then((docs) => {
    console.log('Users: ', JSON.stringify(docs, undefined, 2));
  }, (error) => {
    console.log('Unable to connect mongo servers...', error);
  });

  // db.close();
});
