// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

// var obj = new ObjectID();
// console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err){
    // return console.log('Unable to connect to mongodb server ...');
    return console.log(err);
  }

  console.log('Connect to Mongodb server ...');

  // deleteMany
  // db.collection('Users').deleteMany({name: 'parsa'}).then((result) => {
  //   console.log(result);
  // });


  //deleteOne
  // db.collection('Todos').deleteOne({_id: new ObjectID("5a7dfb53ddc4bee4529d20bf")}).then((result) => {
  //   console.log(result);
  // });


  //findAndDelete
  db.collection('Users').findOneAndDelete({
    _id: new ObjectID("5a7dfb74ddc4bee4529d20d1")
  }).then((result) => {
    console.log(JSON.stringify(result, undefined, 2));
  });

  // db.close();
});
