// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

// var obj = new ObjectID();
// console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err){
    // return console.log('Unable to connect to mongodb server ...');
    return console.log(err);
  }

  console.log('Connect to Mongodb server ...');

  // db.collection('Todos').insert({
  //   text: 'do something',
  //   completed: false
  // }, (err, res) => {
  //   if (err) return console.log('Unable to insert to Todos', err);
  //
  //   console.log(JSON.stringify(res.ops, undefined, 2));
  // })

  // db.collection('Users').insertOne({
  //   name: 'parsa',
  //   age: 20,
  //   location: 'tehran',
  //   university: 'AUT',
  //   major: 'CE'
  // }, (err, res) => {
  //   if (err) return console.log('Unable to connect to mongodb server...');
  //
  //   console.log(res.ops[0]._id.getTimestamp());
  //   // console.log(JSON.stringify(res.ops, undefined, 2));
  // });


  db.close();
});
