// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

// var obj = new ObjectID();
// console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err){
    // return console.log('Unable to connect to mongodb server ...');
    return console.log(err);
  }

  console.log('Connect to Mongodb server ...');

  db.collection('Users').findOneAndUpdate({
    _id: new ObjectID("5a7d5bb24d1d844a2c153214")
  }, {
    $set: {name: 'parsa'},
    $inc: {age: 1}
  }, {
    returnOriginal: false
  }).then((res) => {
    console.log(res);
  })

  // db.close();
});
