var mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

var UserSchema = new mongoose.Schema({
  email: {
    type: 'String',
    required: true,
    trim: true,
    minlength: 5,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email.'
    }
  },
  password: {
    type: 'String',
    required: true,
    minlength: 6
  },
  tokens: [{
    access: {
      type: 'String',
      required: true
    },
    token: {
      type: 'String',
      required: true
    }
  }]
});

UserSchema.pre('save', function(next) {
  var user = this;
  if(user.isModified('password')){
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(user.password, salt, (err, hash) => {
          console.log(hash);
          user.password = hash;
          next();
        });
      });

  } else {
    next();
  }
});

UserSchema.methods.toJSON = function() {
  var user = this;
  var userObject = user.toObject();

  return _.pick(userObject, ['_id', 'email']);
};


UserSchema.methods.generateAuthToken =  function() {
  var user = this;
  // console.log(this);
  var access = 'auth';

  var token = jwt.sign({_id: user._id.toHexString(), access}, 'abc123').toString();
  user.tokens.push({access, token});
  // user.findOneAndUpdate({
  //
  // })

  // return token;
  // await user.save();
  return token;
  // return user.save().then(() => token);
};

UserSchema.statics.findByToken = function (token){
  var User = this;
  var decoded;

  try{
    decoded = jwt.verify(token, 'abc123');

  } catch(e){
    // return new Promise((resolve, reject) => {
    return Promise.reject();
  };



  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  });

  // return user;
};

var User = mongoose.model('Users', UserSchema);


module.exports = {User};
