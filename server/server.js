const {ObjectID} = require('mongodb');

const _ = require('lodash');
var express = require('express');
var bodyParser = require('body-parser');

var {mongoose} = require('./db/mongoose');
var {Todo} = require('./models/todo');
var {User} = require('./models/user');
var {authenticate} = require('./middleware/authenticate')
// const validator = require('validator');


var app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());

app.post('/todos', (req, res) => {
  var todo = new Todo({
    text: req.body.text
  });

  todo.save().then((doc) => {
    res.send(doc);
  }, (error) => {
    res.status(400).send(error);
  })
});

app.post('/users', (req, res) => {
  // var user = new User({
  //   email: req.body.text,
  //   password: req.body.password/nk
  // });
  var body = _.pick(req.body, ['email', 'password']);
  var user = new User(body);

  // await user.save();
  var token = user.generateAuthToken();
  user.save().then(() => {
    // return user.generateAuthToken();
    // var token = user.generateAuthToken();
    res.header('x-auth', token).send(user);
  })
  // .then((token) => {
  //   res.header('x-auth', token).send(user);
  // })
  .catch((e) => {
    res.status(400).send(e);
  })
});



app.get('/users/me',authenticate , (req, res) => {
  res.send(req.user);
});

app.get('/todos', (req, res) => {
  Todo.find().then((todos) => {
    res.send({todos});
  }, (error) => {
    res.status(400).send(error);
  })
});

app.delete('/todos/:id', (req, res) => {
  var id = req.params.id;

  if(!ObjectID.isValid(id)){
    res.status(404).send();
  };

  Todo.findByIdAndRemove(id).then((todo) => {
    if(todo){
      res.send({todo});
    } else res.status(404).send();
  }, (error) => {
    res.status(400).send();
  });
});

app.patch('/todos/:id', (req, res) => {
  var id = req.params.id;
  var body = _.pick(req.body, ['text', 'completed']);

  if(!ObjectID.isValid(id)){
    res.status(404).send();
  };

  if(_.isBoolean(body.completed) && body.completed){
    body.completedAt = new Date().getTime();
  } else{
    body.completed = false;
    body.completedAt = null;
  }

  Todo.findByIdAndUpdate(id, {$set: body}, {new: true}).then((todo) => {
    if(!todo){
      return res.status(404).send();
    }
    res.send({todo});
  }).catch((e) => {
    res.status(400).send();
  });
});

app.get('/todos/:id', (req, res) => {

  var id = req.params.id;
  //  valid id using isValid
    //  404
  //  findById
    //  success
      //  if todo - send it back
      //  if not 404
    // error 400
  if(!ObjectID.isValid(id)){
    res.status(404).send();
  };

  Todo.findById(id).then((todo) => {
    if(todo){
      res.send({todo});
    } else res.status(404).send();
  }, (error) => {
    res.status(400).send();
  });

});

app.listen(port, () => {
  console.log(`Server is up on port ${port}`);
});


module.exports = {app};
